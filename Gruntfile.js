module.exports = function(grunt) {
  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },
    jade: {
        target: {
            files: [{
                expand: true,
                cwd: 'src/',
                src: '**/*.jade',
                dest: 'src',
                ext: '.html'
            }]
        }
    },
    htmlmin: {        
        target: {
            options: {                                 
                removeComments: true,
                collapseWhitespace: true,
                removeTagWhitespace: true,
                removeRedundantAttributes: true,
                removeEmptyAttributes: true,
                removeScriptTypeAttributes: true,
                removeEmptyElements: true,
                minifyJS: true,
                minifyCSS: true

            },
            files: [{
                  expand: true,
                  cwd: 'src/',
                  src: '**/*.html',
                  dest: 'build/'
              }]
        }
    },
    sass: {
        target: {
            files: [{
                expand: true,
                cwd: 'src/',
                src: '**/*.scss',
                dest: 'src/',
                ext: '.css'
            }]
        }
    },
	cssmin: {
        target: {
            files: [{
                  expand: true,
                  cwd: 'src/',
                  src: '**/*.css',
                  dest: 'build/'
              }]
        }
    },
    uglify: {
        options: {
          compress: {
            drop_console: true
          }
        },
        target: {
            files: [{
                  expand: true,
                  cwd: 'src/',
                  src: '**/*.js',
                  dest: 'build/'
              }]
        }
    },
    watch: {
        configFiles: {
            files: [ 'Gruntfile.js', 'config/*.js' ],
            options: {
                reload: true
            }
        },
	    jade:{
		    files:"src/**/*.jade",
		    tasks:['jade']
	    },
	    scss: {
	        files: "src/**/*.scss",
	        tasks: ['sass', 'cssmin']
	    },
	    js:{
		    files:"src/**/*.js",
		    tasks:['uglify','jshint']
	    },
	    html:{
		    files:"src/**/*.html",
		    tasks:"htmlmin"
	    }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-jade');

  grunt.registerTask('default', ['watch']);
};
